import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherPredictComponent } from './weather-predict.component';

describe('WeatherPredictComponent', () => {
  let component: WeatherPredictComponent;
  let fixture: ComponentFixture<WeatherPredictComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WeatherPredictComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherPredictComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
