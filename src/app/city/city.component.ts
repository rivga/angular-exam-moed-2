import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  position: number;
  name: string;
  
  
  
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'London'},
  {position: 2, name: 'Paris'},
  {position: 3, name: 'Tel Aviv'},
  {position: 4, name: 'Jerusalem'},
  {position: 5, name: 'Berlin'},
  {position: 6, name: 'Rome'},
  {position: 7, name: 'Dubai'},
  {position: 8, name: 'Athenes'},
  
];

/**
 * @title Basic use of `<table mat-table>`
 */
@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css']
})
export class CityComponent implements OnInit {
  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  
  displayedColumns: string[] = ['position', 'name','getdetails'];
  dataSource = ELEMENT_DATA;
}

