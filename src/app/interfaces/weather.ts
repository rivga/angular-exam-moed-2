export interface Weather {
    name:string,
    country:string,
    description:string,
    temperature:number
}
