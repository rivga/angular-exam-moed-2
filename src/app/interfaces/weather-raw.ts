export interface WeatherRaw {
    
    weather:[
       {
          description:string
          icon:string
       }
    ],
    main:{
       temp:number
    },
    sys:{
       country:string
    },
    name:string
}
