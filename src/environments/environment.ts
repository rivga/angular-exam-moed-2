// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyC2kU22_B8t_qdPPLNY1SbMfrCgbs5SG5w",
    authDomain: "myexam-48450.firebaseapp.com",
    projectId: "myexam-48450",
    storageBucket: "myexam-48450.appspot.com",
    messagingSenderId: "142719938632",
    appId: "1:142719938632:web:cea639a8210d76db15287d"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
